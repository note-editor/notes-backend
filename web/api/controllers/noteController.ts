"use strict";
import mongoose = require("mongoose");
import { formatErrorResponse, formatResponse } from "../../utilities/response";
import { INoteModel } from "../models/noteModel";

const Note = mongoose.model("Note");

exports.create = async (req: any, res: any ) => {
  req.body.user = req.userID;
  const newNote = new Note(req.body);

  let result: INoteModel;
  try {
    result = await newNote.save();
  } catch (err) {
    res.json(formatErrorResponse(err));
    return;
  }

  res.json(formatResponse(result));
  return;
};

exports.get_by_user = async (req: any, res: any) => {
  let notes: INoteModel[];

  try {
    notes = await Note.find({user: req.userID});
  } catch (err) {
    res.json(err); // TODO format error
    return;
  }

  res.json(formatResponse(notes));
  return;
};

exports.delete = async (req: any, res: any) => {
  let result: any;

  try {
    result = await Note.deleteOne({user: req.userID, _id: req.params.id});
  } catch (err) {
    res.json(err); // TODO format error
    return;
  }

  res.json(formatResponse(result));
  return;
};

exports.update = async (req: any, res: any) => {
  let result: INoteModel;

  try {
    result = await Note.update({ _id: req.params.id }, { $set: req.body} );
  } catch (err) {
    res.json(err); // TODO format error
    return;
  }

  res.json(formatResponse(result));
  return;
};
