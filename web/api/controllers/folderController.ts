"use strict";
import mongoose = require("mongoose");
import { formatErrorResponse, formatResponse } from "../../utilities/response";
import { IFolderModel } from "../models/folderModel";

const Folder = mongoose.model("Folder");

exports.create = async (req: any, res: any ) => {
  req.body.user = req.userID;
  const newFolder: IFolderModel = new Folder(req.body);
  let result: IFolderModel;

  try {
    result = await newFolder.save();
  } catch (err) {
    res.json(formatErrorResponse(err));
    return;
  }

  res.json(formatResponse(result));
  return;
};

exports.get_by_user = async (req: any, res: any) => {
  let folders: IFolderModel[];

  try {
    folders = await Folder.find({user: req.userID});
  } catch (err) {
    res.json(formatErrorResponse(err));
    return;
  }

  res.json(formatResponse(folders));
  return;
};

exports.delete = async (req: any, res: any) => {
  let result: any;

  try {
    result = await Folder.deleteOne({user: req.userID, _id: req.params.id});
  } catch (err) {
    res.json(formatErrorResponse(err));
    return;
  }

  res.json(formatResponse(result));
  return;
};

exports.update = async (req: any, res: any) => {
  let result: IFolderModel;

  try {
    result = await Folder.update({ _id: req.params.id }, { $set: req.body} );
  } catch (err) {
    res.json(err); // TODO format error
    return;
  }

  res.json(formatResponse(result));
  return;
};
