"use strict";
import verifyRequest from "../../utilities/auth";

module.exports = (app: any) => {
  const note = require("../controllers/noteController");

  // Use the auth middleware
  app.use(verifyRequest);

  // Register note routes
  app.route("/notes").post(note.create);
  app.route("/notes").get(note.get_by_user);
  app.route("/notes/:id").delete(note.delete);
  app.route("/notes/:id").patch(note.update);
};
