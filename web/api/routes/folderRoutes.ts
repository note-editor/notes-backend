"use strict";
import verifyRequest from "../../utilities/auth";

module.exports = (app: any) => {
  const folder = require("../controllers/folderController");

  // Use the auth middleware
  app.use(verifyRequest);

  // Register Route
  app.route("/folders").post(folder.create);
  app.route("/folders").get(folder.get_by_user);
  app.route("/folders/:id").delete(folder.delete);
  app.route("/folders/:id").patch(folder.update);
};
