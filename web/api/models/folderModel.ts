"use strict";
import mongoose = require("mongoose");
import { Document, Model, model} from "mongoose";
import createError from "../../utilities/error";

const Schema = mongoose.Schema;

interface IFolder {
  createdAt?: Date;
  parent?: mongoose.Schema.Types.ObjectId;
  tags?: string;
  title?: string;
  updatedAt?: Date;
  user?: string;
}

export interface IFolderModel extends IFolder, Document {
  createdAt?: Date;
  parent?: mongoose.Schema.Types.ObjectId;
  tags?: string;
  title?: string;
  updatedAt?: Date;
  user?: string;
}

const folderSchema = new Schema(
  {
    createdAt: {
      default: Date.now,
      type: Date,
    },
    parent: {
      default: null,
      ref: "Folder",
      type: Schema.Types.ObjectId,
    },
    tags: {
      default: "",
      type: String,
    },
    title: {
      default: "",
      type: String,
    },
    updatedAt: {
      default: Date.now,
      type: Date,
    },
    user: {
      required: createError("nb-e-000001", {
        detail: "user id",
        inputRef: "null",
      }),
      type: String,
    },
  },
  { strict: true },
);

export const Folder: Model<IFolderModel> = model<IFolderModel>("Folder", folderSchema);
