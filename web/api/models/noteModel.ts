"use strict";
import mongoose = require("mongoose");
import { Document, Model, model} from "mongoose";
import createError from "../../utilities/error";

const Schema = mongoose.Schema;

interface INote {
  content?: string;
  createdAt?: Date;
  parent?: mongoose.Schema.Types.ObjectId;
  tags?: string;
  title?: string;
  updatedAt?: number;
  user?: string;
}

export interface INoteModel extends INote, Document {
  content?: string;
  createdAt?: Date;
  parent?: mongoose.Schema.Types.ObjectId;
  tags?: string;
  title?: string;
  updatedAt?: number;
  user?: string;
}

const noteSchema = new Schema(
  {
    content: {
      default: "",
      type: String,
    },
    createdAt: {
      default: Date.now,
      type: Date,
    },
    parent: {
      default: null,
      ref: "Folder",
      type: Schema.Types.ObjectId,
    },
    tags: {
      default: "",
      type: String,
    },
    title: {
      default: "New Note",
      type: String,
    },
    updatedAt: {
      default: Date.now,
      type: Date,
    },
    user: {
      required: createError("nb-e-000001", {
        detail: "user id",
        inputRef: "null",
      }),
      type: String,
    },
  },
  { strict: true },
);

export const Note: Model<INoteModel> = model<INoteModel>("Note", noteSchema);
