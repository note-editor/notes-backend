#!/usr/bin/env node

import bodyParser = require("body-parser");
import cors = require("cors");
import express = require("express");
import mongoose = require("mongoose");

// Loads the enviroment variables
require("dotenv").config();

// Load models
const Note = require("./api/models/noteModel");
const Folder = require("./api/models/folderModel");

// Set up server
const app = express();
const port = process.env.PORT || 4444;

// Enable cors
app.use(cors());
app.options("*", cors());

// mongoose instance connection url connection
const connectionString = "mongodb://" + process.env.DB_USER + ":" + process.env.DB_PASS + "@127.0.0.1:27017/" + process.env.DB_NAME;
mongoose.Promise = global.Promise;
mongoose.connect(connectionString);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Register routes
const noteRoutes = require("./api/routes/noteRoutes"); // importing route
noteRoutes(app);
const folderRoutes = require("./api/routes/folderRoutes"); // importing route
folderRoutes(app);

app.listen(port);

console.log("API server started on: " + port);
