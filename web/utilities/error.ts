const errors = require("../assets/errors.json");

const createError = (code: string, modifiers: any = {}) => {
  let error = JSON.stringify(errors[code]);

  Object.keys(modifiers).forEach((key) => {
    error = error.replace(`$${key}`, modifiers[key]);
  });

  return JSON.parse(error);
};

export default createError;

export const createErrorFromMongoError = (
  mongoError: any,
  inDetail: string,
) => {
  let error = {};

  switch (mongoError.code) {
    case 11000: // Duplicate key attempted to insert
      error = createError("nb-e-000003", {
        detail: inDetail,
      });
      break;
  }

  return error;
};
