import createError from "./error";
import { formatErrorResponse } from "./response";

const fetch = require("node-fetch");

// Sends a token to the auth service to see if it is valid
async function verifyToken(token: string) {
  const url = process.env.AUTH_URL + "/sessions/" + token;
  const res = await fetch(url);
  return await res.json();
}

// Middleware function to check if a function is valid
async function verifyRequest(req: any, res: any, next: any) {
  // pulls the token from the request header.
  const auth = req.headers.authorization;

  // Auth was provided in the request
  if (auth != null) {
    const token = auth.split(" ")[1];
    const result = await verifyToken(token);
    // The bearer token provided was invalid
    if ("errors" in result) {
      res.status(result.errors[0].status);
      res.send({errors: result.errors});
    } else {
      req.userID = result.data.user._id;
      next();
    }
  // No auth was provided.
  } else {
    res.status(401);
    const error = createError("nb-e-000002");
    res.send(error);
  }
}

export default verifyRequest;
