// Formats a response to match the standars
export const formatResponse = (resData: object, resMeta: object = null) => {
  let response = {};

  // Adds the response data into a data key
  response = {
    ...response,
    data: resData,
  };

  // If the response has meta data
  if (resMeta !== null) {
    response = {
      ...response,
      meta: resMeta,
    };
  }

  return response;
};

// Pushes each error into an array
const formatValidationErrors = (errors: any) => {
  const errorsArr: object[] = [];

  Object.keys(errors).forEach((key) => {
    errorsArr.push(errors[key].properties);
  });

  return errorsArr;
};

// Function to correctly format errors to meet standards
export const formatErrorResponse = (resErrors: any) => {
  let errorsArr: object[] = [];

  if ("name" in resErrors) {
    switch (resErrors.name) {
      case "ValidationError":
        errorsArr = formatValidationErrors(resErrors.errors);
        break;
      default:
        break;
    }
  } else {
    errorsArr.push(resErrors);
  }

  const response = {
    errors: errorsArr,
  };

  return response;
};
