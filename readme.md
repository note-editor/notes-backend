# Notes Backend
The node/expressjs backend for the notes editor.

## Prerequisites
### Mongodb
Switch to notes database
```
use notes-db
```
Add user
```
db.createUser(
  {
    user: "notesAdmin",
    pwd: "password",
    roles: [ { role: "read", db: "notes-db" } ]
  }
)
```

## Local installation
1. Run command: `npm install`